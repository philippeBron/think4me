---
layout: post
title:  "Scénario docker"
date:   2018-02-16 17:32:14 +00:00
categories: tuto
---

La première étape est de créer les environnements d'exécution. Pour cela j'utilise **docker-compose** et le fichier `docker-compose.yml`

```

librairie:
  build: site
  ports:
    - "80:80"
  volumes:
    - /home/fifou/Data/gitlab/librAPIE:/var/www/html
  links:
    - database
  net: "bridge"
  dns:
    - 8.8.8.8
    - 4.4.4.4
phpmyadmin:
  image: corbinu/docker-phpmyadmin
  ports:
    - "8080:80"
  environment:
    - MYSQL_USERNAME=root
    - MYSQL_PASSWORD=password
  links:
    - database:mysql
database:
  image: mysql:5.5
  ports:
    - "3306:3306"
  environment:
    - MYSQL_ROOT_PASSWORD=password
    - MYSQL_DATABASE=w9vhoru3_bibliapie
    - MYSQL_USER=w9vhoru3_fifou93
    - MYSQL_PASSWORD=d3ee8t35

```

La configuration réseau `bridge` permet de se connecter à l'internet pour récupérer les informations sur les livres lors de l'ajout d'ouvrages dans la librairies. Sans cette configuration, le conteneur ne peut pas utiliser la connexion de l'hôte.

La partie **apache** est construite à partir d'une *image* **site**. Celle-ci est définie dans un répertoire site au travers du fichier `Dockerfile`

```

FROM php:5.6-apache

RUN docker-php-ext-install mbstring mysql mysqli pdo_mysql

RUN a2enmod rewrite

```

Il s'agit d'une base de serveur **apache 5.6** à qui on ajoute les extensions nécessaires avant de lancer le serveur en mode réécriture des URL.

Le conteneur **phpMyadmin** est optionnel et n'est utilisé que pour se connecter à la base de données en mode graphique.

## Injection du contenu de l'application
### Serveur Apache
Pour le moment le contenu du site web est lié au travers d'un *volume* virtuel qui permet d'utiliser les fichiers qui sont dans le répertoire git local.
Il faudrait idéalement que le serveur récupère cette arborescence au démarrage et la copie en local sur son propre disque.

Une solution est de compresser l'arborescence et de mettre le fichier à disposition en téléchargement. Il faut ensuite le télécharge et décompresser le tout dans le répertoire `/var/www/html`.
J'ai ajouté les ligne suivante pour compléter l'image.

```

RUN apt-get update

RUN apt-get -y install wget

RUN wget https://librapie.bron.ovh/librAPIE.tar.gz --no-check-certificate

RUN tar -C /var/www/html -xzvf librAPIE.tar.gz

RUN rm librAPIE.tar.gz

```

Le problème est que les fichiers font partie de l'image docker. Il faut donc détruire l'image à chaque fois qu'on veut modifier l'application...

La solution est de reporter le téléchargement et l'installation au moment du lancement du conteneur. Pour cela on utilise la directive `ENTRYPOINT` dans le Dockerfile.

```

FROM php:5.6-apache

RUN docker-php-ext-install mbstring mysql mysqli pdo_mysql

RUN a2enmod rewrite

RUN apt-get update && apt-get install -y wget

COPY launchLibrary.sh /root/launchLibrary.sh
RUN chmod 744 /root/launchLibrary.sh

ENTRYPOINT /root/launchLibrary.sh
WORKDIR /root

```

Avec cette méthode, on a une image qui va installer les composants techniques nécessaires et lancer le script `launchLibrary.sh` au démarrage. Reste à résoudre un petit problème :

    Lorsque le script a fini de s'exécuter, il libère son processus et stoppe le conteneur par la même occasion.

Il faut donc prendre garde à finir le script par une commande qui maintienne le conteneur actif. Dans le cas présent, il s'agit de lancer l'exécution du serveur apache en tâche de fond.

```

#! /bin/bash

echo "### état du serveur apache"
service apache2 status

echo "### changement de répertoire"
cd /root

echo "téléchargement"
wget https://librapie.bron.ovh/librAPIE.tar.gz --no-check-certificate
echo "### fin téléchargement"

echo "### décompression"
tar -C /var/www/html -xzvf librAPIE.tar.gz
echo "### fin décompression"

echo "### suppression"
rm librAPIE.tar.gz
echo "### fin suppression"

echo "### démarrage du serveur apache"
apache2 -D FOREGROUND
service apache2 status
echo "### serveur démarré"

```

### Serveur MySQL
Pour la base de données il n'y a pas de procédure automatique et il faut charger manuellement les données à partir d'un dump de la base en utilisant phpMyadmin.

Il reste donc à trouver le moyen de lancer des commandes post-installation (comme pour la configuration de l'image site).
