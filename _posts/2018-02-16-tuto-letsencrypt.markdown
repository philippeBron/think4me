---
layout: post
title:  "How to lestencrypt"
date:   2018-02-16 16:32:14 +00:00
categories: tuto
---

Ce tutoriel explique comment générer des certificats reconnus en utilisant lets'encrypt. Le cas d'utilisation décrit est la création de certificats pour la connexion https via un serveur haproxy.
## Génération des certificats
La première étape est de générer les certificats associés à un nom de domaine ou de sous-domaine. Pour se faire on utilise la commande
```
sudo letsencrypt certonly
```
Pour que la commande fonctionne, il ne faut pas qu'un serveur tourne sur le port 80 et que la machine soit joignable depuis l'internet (rediriger les ports vers cette machine ou la placer temporairement en DMZ).
Une fois le nom du domaine ou du sous-domaine renseigné, un ensemble de fichiers sont créés dans le répertoire `/etc/letsencrypt/archive/<domaine>`. Ces fichiers sont les suivants :

* **cert1.pem** : certificat serveur
* **chain1.pem** : chaine de certification mise en oeuvre pour la création de ce certificat
* **fullchain1.pem** : *à chercher*
* **privkey1.pem** : la clé privée du certificat créé

Pour que le serveur haproxy puisse fonctionner, il faut lui créer un fichier contenant à la fois le certificat public et la clé privé. Pour cela on utilise la commande suivante dans le répertoire contenant les fichiers :
```
cat cert1.pem privkey1.pem > <domaine>.pem
```

## Déploiement des certificats sur haproxy
Le fichier qui vient d'être créé doit être copié dans le répertoire défini au niveau du fichier de configuration `haproxy.cfg`. Cette déclaration se fait au travers de la partie bind :
```
bind *:443 ssl crt /usr/local/etc/haproxy/cert/ no-sslv3
```
Dans cette configuration, le certificat associé aux domaines ou sous-domaines seront présentés automatiquement.
Il faut associer ce répertoire au montage docker pour être en mesure de transférer les certificats dans le conteneur sans avoir à s'y connecter. Un redémarrage du serveur haproxy est nécessaire pour que les certificats mis à jour soient pris en compte.
