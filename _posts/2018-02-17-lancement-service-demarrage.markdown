---
layout: post
title:  "Lancement d'un service au démarrage"
date:   2018-02-17 11:45:14 +00:00
categories: tuto
---

![logo docker](https://blog.docker.com/wp-content/uploads/2013/06/Docker-logo-011-300x232.png)

Le contexte d'exemple de cet article est le lancement au démarrage d'un serveur d'un ensemble de conteneurs docker.
Cet article d'inspire de l'article de la documentation docker [Start containers automatically](https://docs.docker.com/engine/admin/host_integration/).

Pour cela j'utilise le fichier `/etc/systemd/system/hosting.service` :
```
[Unit]
Description=Lancement conteneurs docker
Requires=docker.service
After=docker.service

[Service]
ExecStart=/usr/bin/docker-compose -f /media/DataCenter/dockerData/docker-compose.yml -p hosting up
ExecStartPost=/bin/sleep 1

ExecStop=/usr/bin/docker-compose -f /media/DataCenter/dockerData/docker-compose.yml stop
ExecStopPost=/bin/sleep 1

[Install]
WantedBy=multi-user.target
```
Pour activer cette configuration, il faut mettre à jour le deamon avec la commande `systemctl daemon-reload` et lancer le service `systemctl start hosting.service`.

Pour que le service se lance automatiquement au démarrage, il faut lancer la commande `systemctl enable hosting.service`.
